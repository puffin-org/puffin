package xyz.puffin.bot.listeners;

import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import xyz.puffin.bot.Main;
import xyz.puffin.bot.commands.Command;
import xyz.puffin.bot.managers.GuildManager;
import xyz.puffin.bot.objects.PGuild;
import xyz.puffin.bot.utils.Translator;

import javax.annotation.Nonnull;
import java.awt.*;
import java.util.Arrays;

public class ChatListener extends ListenerAdapter {

    private EventWaiter waiter;

    public ChatListener(EventWaiter waiter) {
        this.waiter = waiter;
    }

    @Override
    public void onGuildMessageReceived(@Nonnull GuildMessageReceivedEvent event) {
        if(event.getAuthor().isBot() || event.getAuthor().isFake() || event.getAuthor() == null || event.getMember() == null || event.getMessage() == null) {
            return;
        }

        String message = event.getMessage().getContentRaw();
        if(message.startsWith("%")) {
            String command = message.substring(1);
            String[] args = new String[0];
            if(message.contains(" ")) {
                command = command.substring(0, message.indexOf(" ") - 1);

                args = message.substring(message.indexOf(" ") + 1).split(" ");
            }

            PGuild guild = GuildManager.getGuildById(event.getGuild().getId());
            for(Command c : Main.getInstance().getCommandHandler().getCommands()) {
                if(command.equals(c.getName())) {

                    if(!event.getGuild().getSelfMember().hasPermission(getBasicPerms())) {
                        return;
                    }

                    if(!event.getMember().hasPermission(c.userPermission())) {
                        return;
                    }

                    if(c.isStaff() && !Main.getInstance().getConfig().getArray("discord.staff").contains(event.getMember().getId())) {
                        return;
                    }

                    if(!event.getGuild().getSelfMember().hasPermission(c.botPermission())) {
                        StringBuilder sb = new StringBuilder();
                        Arrays.stream(c.botPermission()).forEach(p -> sb.append(p.getName()).append(", "));

                        EmbedBuilder builder = new EmbedBuilder();
                        builder.setTitle(Translator.translate("messages.error.permissions.title", guild));
                        builder.setDescription(Translator.translate("messages.error.permissions.description", guild, "`" + sb.toString().substring(0, sb.length() - 2) + "`"));
                        builder.setColor(Color.RED);
                        event.getChannel().sendMessage(builder.build()).queue();
                        return;
                    }

                    try {
                        c.onCommand(event.getMember(), event.getMessage(), event.getChannel(), args, waiter, guild);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    // Delete message after
                    if(c.deleteMessage()) {
                        delete(event.getMessage());
                    }
                }
            }
        }
    }

    /**
     * Delete member's message
     *
     * @param message member's message
     */
    private void delete(@NotNull Message message) {
        if(message.getTextChannel().getGuild().getSelfMember()
                .getPermissions(message.getTextChannel()).contains(Permission.MESSAGE_MANAGE)) {
            message.delete().queue();
        }
    }

    /**
     * Needed default bot's permissions
     *
     * @return needed default bot's permissions
     */
    @NotNull
    @Contract(value = " -> new", pure = true)
    private Permission[] getBasicPerms() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS, Permission.MESSAGE_WRITE};
    }
}
