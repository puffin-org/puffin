package xyz.puffin.bot.listeners;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageType;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import xyz.puffin.bot.managers.GuildManager;
import xyz.puffin.bot.objects.GMember;
import xyz.puffin.bot.objects.PGuild;

public class LevelingListener extends ListenerAdapter {

    @Override
    public void onGuildMessageReceived(@NotNull GuildMessageReceivedEvent event) {

        final Member member = event.getMember();
        final Message message = event.getMessage();

        if(event.getGuild().getId().equals("439385188615716865")) return;
        
        if(message.getType() == null || member == null || member.getUser() == null || message.getType() != MessageType.DEFAULT || member.getUser().isBot() || member.getUser().isFake()) {
            return;
        }

        PGuild guild = GuildManager.getGuildById(event.getGuild().getId());

        if(guild.getMemberById(member.getId()) == null) return;

        GMember gmember = guild.getMemberById(member.getId());

        int lvl = gmember.getLeveling().getLevel();
        if(lvl >= 100) return;

        if(!gmember.getLeveling().canGainXP(message.getTimeCreated())) {
            return;
        } else gmember.getLeveling().updateLastMessage(message.getTimeCreated());

        gmember.getLeveling().updateLeveling(event.getChannel());
    }
}