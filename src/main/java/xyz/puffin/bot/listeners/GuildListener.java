package xyz.puffin.bot.listeners;

import net.dv8tion.jda.api.entities.Invite;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import xyz.puffin.bot.managers.GuildManager;
import xyz.puffin.bot.utils.PuffinLogger;

import javax.annotation.Nonnull;

public class GuildListener extends ListenerAdapter {

    @Override
    public void onGuildMemberJoin(@Nonnull GuildMemberJoinEvent event) {
        if(GuildManager.getGuildById(event.getGuild().getId()) == null) return;
        GuildManager.getGuildById(event.getGuild().getId()).memberJoin(event.getMember());
    }

    @Override
    public void onGuildMemberLeave(@Nonnull GuildMemberLeaveEvent event) {
        if(GuildManager.getGuildById(event.getGuild().getId()) == null) return;
        GuildManager.getGuildById(event.getGuild().getId()).memberLeave(event.getMember());
    }

    @Override
    public void onGuildJoin(@Nonnull GuildJoinEvent event) {
        PuffinLogger.infoMessage("Joining guild ``" + event.getGuild().getName() + "``");
        GuildManager.botJoin(event.getGuild());
    }

    @Override
    public void onGuildLeave(@Nonnull GuildLeaveEvent event) {
        PuffinLogger.infoMessage("Leaving guild ``" + event.getGuild().getName() + "``");
        GuildManager.botLeave(event.getGuild());
    }
}
