package xyz.puffin.bot.sql;

import com.zaxxer.hikari.HikariDataSource;
import org.jetbrains.annotations.Nullable;
import xyz.puffin.bot.Main;
import xyz.puffin.bot.objects.GMember;
import xyz.puffin.bot.objects.PGuild;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SQLManager {

    private final Main puffin;
    private final ConnectionPoolManager pool;
    private HikariDataSource dataSource;

    public SQLManager(Main puffin) {
        this.puffin = puffin;
        pool = new ConnectionPoolManager(puffin, "MySQL-Pool");
    }

    public ConnectionPoolManager getPool() {
        return pool;
    }

    /**
     * Creates a guild entry in the database
     *
     * @param guildId guild's id
     * @return guild's data
     */
    @Nullable
    public final PGuild createGuild(final String guildId) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = pool.getConnection();
            ps = conn.prepareStatement("INSERT INTO `guilds`(`guild_id`) VALUES (?);", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, guildId);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if(rs.next()) {
                return new PGuild(rs.getLong(1), guildId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pool.close(conn, ps, null);
        }
        return null;
    }

    /**
     * Returns guild where is puffin
     *
     * @param guildId guild's id
     * @return guild where is puffin
     */
    public PGuild getGuild(final String guildId) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = pool.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `guilds` WHERE guild_id = ?;");
            ps.setString(1, guildId);
            ps.executeQuery();
            return new PGuild(ps.getResultSet().getLong("id"), guildId);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pool.close(conn, ps, null);
        }
        return null;
    }

    /**
     * Removes guild from the database
     *
     * @param guildId guild's id
     */
    @Nullable
    public final void deleteGuild(final String guildId) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = pool.getConnection();
            ps = conn.prepareStatement("DELETE FROM `guilds` WHERE guild_id = ?;");
            ps.setString(1, guildId);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pool.close(conn, ps, null);
        }
    }

    /**
     * Creates a member entry in the database
     *
     * @param databaseId guild's database id
     * @return member's data
     */
    @Nullable
    public final GMember createMember(final long databaseId, final String memberId) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = pool.getConnection();
            ps = conn.prepareStatement("INSERT INTO `members` (`guild_id`, `member_id`) VALUES (?, ?);", Statement.RETURN_GENERATED_KEYS);
            ps.setLong(1, databaseId);
            ps.setString(2, memberId);
            ps.executeUpdate();
            return new GMember(memberId);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pool.close(conn, ps, null);
        }
        return null;
    }

    /**
     * Updates a member leveling in the database
     *
     * @param databaseId guild's database id
     * @param memberId   member's id
     * @param level      member's level
     * @param experience member's experience
     */
    public final void updateMembersLeveling(final long databaseId, final String memberId, final long level, final long experience) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = pool.getConnection();
            ps = conn.prepareStatement("UPDATE `members` SET `level`= ?, `xp`=? WHERE (`member_id` = ? AND `guild_id` = ?);");
            ps.setLong(1, level);
            ps.setLong(2, experience);
            ps.setString(3, memberId);
            ps.setLong(4, databaseId);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pool.close(conn, ps, null);
        }
    }

    /**
     * Returns list of members in a guild
     *
     * @param databaseId guild's database id
     * @return list of members in a guild
     */
    public List<GMember> getMembers(final long databaseId) {
        List<GMember> list = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = pool.getConnection();
            ps = conn.prepareStatement("SELECT member.*, guild.guild_id FROM `members` AS `member` JOIN `guilds` AS `guild` ON guild.id = member.guild_id WHERE member.guild_id = ?;");
            ps.setLong(1, databaseId);
            ps.executeQuery();
            while (ps.getResultSet().next()) {
                GMember member = new GMember(ps.getResultSet().getString("member_id")).setGuildId(ps.getResultSet().getString("guild_id"));
                member.getLeveling().setLevel(ps.getResultSet().getInt("level")).setExperience(ps.getResultSet().getInt("xp"));
                list.add(member);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pool.close(conn, ps, null);
        }
        return list;
    }

    /**
     * Returns guild's member
     *
     * @param databaseId guild's database id
     * @return guild's member
     */
    public GMember getMember(final long databaseId, final String memberId) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = pool.getConnection();
            ps = conn.prepareStatement("SELECT member.*, guild.guild_id FROM `members` AS `member` JOIN `guilds` AS `guild` ON guild.id = member.guild_id WHERE member.guild_id = ? AND member.member_id = ?;");
            ps.setLong(1, databaseId);
            ps.setString(2, memberId);
            ps.executeQuery();
            GMember member = new GMember(ps.getResultSet().getString("member_id")).setGuildId(ps.getResultSet().getString("guild_id"));
            member.getLeveling().setLevel(ps.getResultSet().getInt("level")).setExperience(ps.getResultSet().getInt("xp"));
            return member;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pool.close(conn, ps, null);
        }
        return null;
    }

    /**
     * Returns list of guilds where is puffin
     *
     * @return list of guilds where is puffin
     */
    public List<PGuild> getGuilds() {
        List<PGuild> list = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = pool.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `guilds`");
            ps.executeQuery();
            while (ps.getResultSet().next()) {
                list.add(new PGuild(ps.getResultSet().getLong("id"), ps.getResultSet().getString("guild_id")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pool.close(conn, ps, null);
        }
        return list;
    }

    /**
     * Checks if guild's data is exists
     *
     * @param guildId guild's id
     * @return the existence of the guild
     */
    public boolean existsGuildData(final String guildId) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = pool.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `guilds` WHERE `guild_id` = ?");
            ps.setString(1, guildId);
            ps.executeQuery();
            return ps.getResultSet().next();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pool.close(conn, ps, null);
        }
        return false;
    }

    /**
     * Checks if member's data is exists
     *
     * @param databaseId guild's database id
     * @param memberId   members's id
     * @return the existence of the guild
     */
    public boolean existsMemberData(final long databaseId, final String memberId) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = pool.getConnection();
            ps = conn.prepareStatement("SELECT * FROM members WHERE guild_id = ? AND member_id = ?");
            ps.setLong(1, databaseId);
            ps.setString(2, memberId);
            ps.executeQuery();
            return ps.getResultSet().next();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pool.close(conn, ps, null);
        }
        return false;
    }
}
