package xyz.puffin.bot;

import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xyz.puffin.bot.commands.CommandHandler;
import xyz.puffin.bot.listeners.ChatListener;
import xyz.puffin.bot.listeners.GuildListener;
import xyz.puffin.bot.listeners.LevelingListener;
import xyz.puffin.bot.managers.GuildManager;
import xyz.puffin.bot.managers.LanguageManager;
import xyz.puffin.bot.sql.SQLManager;
import xyz.puffin.bot.utils.config.Config;
import xyz.puffin.bot.utils.config.ConfigUtils;
import xyz.puffin.bot.utils.tasks.StatusChanger;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.ConcurrentHashMap;

public class Main {

    public static final Logger LOGGER;
    private static final Map<String, Logger> LOGGERS;
    private static Main instance;
    private static JDA jda;
    private static Config config = new ConfigUtils().loadConfig();

    static {
        new File("logs/latest.log").renameTo(new File("logs/log-" + getCurrentTimeStamp() + ".log"));
        LOGGERS = new ConcurrentHashMap<>();
        LOGGER = getLog(Main.class);
    }

    private CommandHandler ch = new CommandHandler();
    private SQLManager sql;

    public static void main(String[] args) throws LoginException, InterruptedException {

        EventWaiter waiter = new EventWaiter();

        jda = new JDABuilder(AccountType.BOT)
                .setToken(config.getString("discord.token"))
                .addEventListeners(waiter)
                .addEventListeners(new ChatListener(waiter))
                .addEventListeners(new LevelingListener())
                .addEventListeners(new GuildListener())
                .setStatus(OnlineStatus.IDLE)
                .setActivity(Activity.playing("Loading"))
                .build().awaitReady();

        (instance = new Main()).initDatabase();

        // Loads all languages
        LanguageManager.loadLanguages();

        // Loads all guilds
        GuildManager.loadGuilds();

        instance.init();

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new StatusChanger(), 10, 10000);
        jda.getPresence().setStatus(OnlineStatus.ONLINE);
    }

    public static Main getInstance() {
        return instance;
    }

    public static JDA getJda() {
        return jda;
    }

    private static Logger getLog(String name) {
        return LOGGERS.computeIfAbsent(name, LoggerFactory::getLogger);
    }

    public static Logger getLog(Class<?> clazz) {
        return getLog(clazz.getName());
    }

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        Date now = new Date();
        return sdfDate.format(now);
    }

    private void init() {
        ch.register();
    }

    private void initDatabase() {
        sql = new SQLManager(this);
    }

    public SQLManager getSql() {
        return sql;
    }

    public Config getConfig() {
        return config;
    }

    public CommandHandler getCommandHandler() {
        return ch;
    }


}
