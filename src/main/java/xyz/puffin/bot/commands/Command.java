package xyz.puffin.bot.commands;

import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import xyz.puffin.bot.objects.PGuild;

public interface Command {

    /**
     * Runs when command is executed
     *
     * @param member  guild's member
     * @param message members's message
     * @param channel guild's channel
     * @param args    message's arguments
     * @param waiter  event waiter
     * @param guild   guild's data
     */
    void onCommand(Member member, Message message, TextChannel channel, String[] args, EventWaiter waiter, PGuild guild);

    /**
     * Command's name
     *
     * @return command name
     */
    String getName();

    /**
     * Command's type
     *
     * @return command type
     */
    CommandType getType();


    /**
     * Needed user's permissions
     *
     * @return needed user permissions
     */
    Permission[] userPermission();


    /**
     * Needed bot's permissions
     *
     * @return needed bot permissions
     */
    Permission[] botPermission();

    /**
     * Command's aliases
     *
     * @return command aliases
     */
    default String[] getAliases() {
        return new String[]{};
    }

    /**
     * Delete user's message when execute a command
     *
     * @return delete message
     */
    default boolean deleteMessage() {
        return false;
    }

    /**
     * Staff's commands can use only staff's members
     *
     * @return if command is for staffs
     */
    default boolean isStaff() {
        return false;
    }
}
