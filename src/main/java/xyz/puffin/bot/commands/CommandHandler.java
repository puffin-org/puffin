package xyz.puffin.bot.commands;

import org.reflections.Reflections;
import xyz.puffin.bot.utils.PuffinLogger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CommandHandler {

    // List of registered command
    private List<Command> commands = new ArrayList<>();

    /**
     * Registers command for use
     *
     * @param command command for register
     */
    private void registerCommand(Command command) {
        try {
            commands.add(command);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Returns list of registered commands
     *
     * @return list of commands
     */
    public List<Command> getCommands() {
        return commands;
    }

    /**
     * Returns list of registered commands filtered by type
     *
     * @param type command's type
     * @return list of commands
     */
    public List<Command> getCommandsByType(CommandType type) {
        return commands.stream().filter(t -> t.getType().equals(type)).collect(Collectors.toList());
    }

    /**
     * Register all commands in package xyz.puffin.bot.commands
     */
    public void register() {
        PuffinLogger.infoMessage("Loading all commands");
        try {
            Reflections reflections = new Reflections("xyz.puffin.bot.commands");
            for(Class<? extends Command> c : reflections.getSubTypesOf(Command.class)) {
                registerCommand(c.newInstance());
            }
        } catch (Exception e) {
            PuffinLogger.fatalMessage("Error while registering command");
            e.printStackTrace();
        }

        PuffinLogger.greatMessage("DiscordiaBot will respond to (" + commands.size() + ") commands.");
    }

}
