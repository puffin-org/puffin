package xyz.puffin.bot.commands.staff;

import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.jagrosh.jdautilities.menu.Paginator;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.exceptions.PermissionException;
import xyz.puffin.bot.Main;
import xyz.puffin.bot.commands.Command;
import xyz.puffin.bot.commands.CommandType;
import xyz.puffin.bot.objects.PGuild;

import java.util.concurrent.TimeUnit;

public class GuildsCommand implements Command {

    @Override
    public void onCommand(Member member, Message message, TextChannel channel, String[] args, EventWaiter waiter, PGuild guild) {
        Paginator.Builder builder = new Paginator.Builder()
                .setColumns(1)
                .setText("Guilds where is puffin")
                .setItemsPerPage(10)
                .showPageNumbers(false)
                .waitOnSinglePage(false)
                .useNumberedItems(true)
                .setFinalAction(m -> {
                    try {
                        m.clearReactions().queue();
                    } catch (PermissionException e) {
                        m.delete().queue();
                    }
                })
                .setEventWaiter(waiter)
                .setTimeout(1, TimeUnit.MINUTES);

        for(Guild g : Main.getJda().getGuilds()) {
            builder.addItems("Name: " + g.getName() + ", Id: " + g.getId() + ", Owner: " + g.getOwner().getUser().getName() + ", Members:" + g.getMembers().size());
        }

        builder.build().paginate(channel, 1);
    }

    @Override
    public String getName() {
        return "guilds";
    }

    @Override
    public CommandType getType() {
        return CommandType.ADMINISTRATION;
    }

    @Override
    public Permission[] userPermission() {
        return new Permission[0];
    }

    @Override
    public Permission[] botPermission() {
        return new Permission[0];
    }

    @Override
    public boolean isStaff() {
        return true;
    }
}
