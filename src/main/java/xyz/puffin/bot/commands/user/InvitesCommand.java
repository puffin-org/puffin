package xyz.puffin.bot.commands.user;

import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.jagrosh.jdautilities.menu.Paginator;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Invite;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.exceptions.PermissionException;
import xyz.puffin.bot.commands.Command;
import xyz.puffin.bot.commands.CommandType;
import xyz.puffin.bot.objects.PGuild;
import xyz.puffin.bot.utils.MessageUtils;
import xyz.puffin.bot.utils.Translator;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class InvitesCommand implements Command {

    @Override
    public void onCommand(Member member, Message message, TextChannel channel, String[] args, EventWaiter waiter, PGuild guild) {
        if(args.length < 1) {
            HashMap<String, Integer> inviters = new HashMap<>();
            Paginator.Builder builder = new Paginator.Builder()
                    .setColumns(1)
                    .setText(Translator.translate(this, "messages.title", guild))
                    .setItemsPerPage(10)
                    .showPageNumbers(true)
                    .waitOnSinglePage(false)
                    .useNumberedItems(true)
                    .setFinalAction(m -> {
                        try {
                            m.clearReactions().queue();
                        } catch (PermissionException e) {
                            m.delete().queue();
                        }
                    })
                    .setEventWaiter(waiter)
                    .setTimeout(1, TimeUnit.MINUTES);
            for(Invite invite : member.getGuild().retrieveInvites().complete()) {
                if(invite.getInviter() == null) continue;
                if(!inviters.containsKey(invite.getInviter().getId())) {
                    inviters.put(invite.getInviter().getId(), invite.getUses());
                } else {
                    inviters.replace(invite.getInviter().getId(), inviters.get(invite.getInviter().getId()) + invite.getUses());
                }
            }

            Object[] a = inviters.entrySet().toArray();
            Arrays.sort(a, (Comparator) (o1, o2) -> ((Map.Entry<String, Integer>) o2).getValue()
                    .compareTo(((Map.Entry<String, Integer>) o1).getValue()));

            for(Object object : a) {
                Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>) object;
                builder.addItems(Translator.translate(this, "messages.invited_list", guild, member.getGuild().getMemberById(entry.getKey()) == null ? entry.getKey() : member.getGuild().getMemberById(entry.getKey()).getUser().getName(), entry.getValue()));
            }
            
            builder.build().paginate(channel, 1);
            return;
        }
        if(message.getMentionedMembers().size() < 1) {
            channel.sendMessage(MessageUtils.getHelpMessage(this, guild));
            return;
        }

        int invites = 0;
        for(Invite invite : member.getGuild().retrieveInvites().complete()) {
            if(invite.getInviter() == null) continue;
            if(invite.getInviter().getId().equals(message.getMentionedMembers().get(0).getId())) {
                invites += invite.getUses();
            }
        }
        channel.sendMessage(Translator.translate(this, "messages.invited", guild, message.getMentionedMembers().get(0).getUser().getName(), invites)).queue();
    }

    @Override
    public String getName() {
        return "invites";
    }

    @Override
    public CommandType getType() {
        return CommandType.GENERAL;
    }

    @Override
    public Permission[] userPermission() {
        return new Permission[0];
    }

    @Override
    public Permission[] botPermission() {
        return new Permission[]{Permission.MANAGE_SERVER};
    }
}
