package xyz.puffin.bot.commands.user;

import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDAInfo;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import xyz.puffin.bot.Main;
import xyz.puffin.bot.commands.Command;
import xyz.puffin.bot.commands.CommandType;
import xyz.puffin.bot.objects.PGuild;
import xyz.puffin.bot.utils.CPUDaemon;
import xyz.puffin.bot.utils.Constants;
import xyz.puffin.bot.utils.Translator;

import java.lang.management.ManagementFactory;

public class StatsCommand implements Command {

    @Override
    public void onCommand(Member member, Message message, TextChannel channel, String[] args, EventWaiter waiter, PGuild guild) {
        long totalMb = Runtime.getRuntime().totalMemory() / (1024 * 1024);
        long usedMb = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024 * 1024);
        EmbedBuilder embed = new EmbedBuilder();
        embed.setDescription(Translator.translate(this, "messages.awake", guild, getUptime(guild)));
        embed.addField(Constants.HOMES + " " + Translator.translate(this, "messages.stats.guilds", guild), String.valueOf(channel.getJDA().getGuilds().size()), true);
        embed.addField(Constants.USERS + " " + Translator.translate(this, "messages.stats.users", guild), String.valueOf(channel.getJDA().getUsers().size()), true);
        embed.addField(Constants.PENCIL + " " + Translator.translate(this, "messages.stats.text-channels", guild), String.valueOf(channel.getJDA().getTextChannels().size()), true);
        embed.addField(Constants.MEGAFON + " " + Translator.translate(this, "messages.stats.voice-channels", guild), String.valueOf(channel.getJDA().getVoiceChannels().size()), true);
        embed.addField(Constants.JDA + " " + Translator.translate(this, "messages.stats.jda-version", guild), JDAInfo.VERSION, true);
        embed.addField(Constants.COMPRESS + " " + Translator.translate(this, "messages.stats.load-cpu", guild), ((int) (CPUDaemon.get() * 10000)) / 100d + "%", true);
        embed.addField(Constants.FLOPY_DISC + " " + Translator.translate(this, "messages.stats.memory", guild), usedMb + "MB / " + totalMb + "MB", true);
        embed.addField(Constants.COMET + " " + Translator.translate(this, "messages.stats.threads", guild), String.valueOf(Thread.getAllStackTraces().size()), true);
        embed.addField(Constants.JAVA + " " + Translator.translate(this, "messages.stats.java", guild), System.getProperty("java.version"), true);
        embed.setAuthor(Translator.translate(this, "messages.title", guild), null, Main.getJda().getSelfUser().getAvatarUrl());
        embed.setColor(Constants.DEFAULT_GREEN_COLOR);
        channel.sendMessage(embed.build()).queue();
    }

    @Override
    public String getName() {
        return "stats";
    }

    @Override
    public CommandType getType() {
        return CommandType.GENERAL;
    }

    @Override
    public Permission[] userPermission() {
        return new Permission[0];
    }

    @Override
    public Permission[] botPermission() {
        return new Permission[0];
    }

    private String getUptime(PGuild guild) {
        long millis = ManagementFactory.getRuntimeMXBean().getUptime();
        long seconds = millis / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;
        return Translator.translate(this, "messages.uptime", guild, days, hours, minutes);
    }
}
