package xyz.puffin.bot.commands.user;

import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import org.jetbrains.annotations.NotNull;
import xyz.puffin.bot.commands.Command;
import xyz.puffin.bot.commands.CommandType;
import xyz.puffin.bot.managers.GuildManager;
import xyz.puffin.bot.objects.GMember;
import xyz.puffin.bot.objects.PGuild;
import xyz.puffin.bot.utils.Constants;
import xyz.puffin.bot.utils.PuffinLogger;
import xyz.puffin.bot.utils.Translator;
import xyz.puffin.bot.utils.Utils;
import xyz.puffin.bot.utils.graphics.GraphicsUtils;

import javax.annotation.Nullable;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class RankCommand implements Command {

    @Nullable
    public static File getRankCard(@NotNull GMember member) {
        try {
            // Default image for painting
            BufferedImage picture = new BufferedImage(934, 282, BufferedImage.TYPE_INT_RGB);

            // Member's avatar
            BufferedImage avatar = Utils.getAvatar(member.getMember());

            // Member's guild
            PGuild guild = GuildManager.getGuildById(member.getGuild().getGuild().getId());

            // Graphics in 2D for painting
            Graphics2D g = (Graphics2D) picture.getGraphics();

            // RenderingHints for better quality
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
            g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

            // Draw background to card
            GraphicsUtils.setBackground(g, picture);

            // Progress bar with Xps
            GraphicsUtils.drawProgressBar(g, member.getLeveling().getExperience(), member.getLeveling().getNeededExperiences(), new GradientPaint(0, 0, Color.decode("#129BCA"), picture.getWidth() - Utils.getValueFromPercent(picture.getWidth(), 30), 0, Color.decode("#6CAF82"), true), picture);

            // Rounded avatar image
            g.drawImage(GraphicsUtils.makeRoundedCorner(avatar, avatar.getWidth()), 30, ((picture.getHeight() - 160) / 2), 160, 160, null);

            // Rounded status image
            g.drawImage(GraphicsUtils.getStatusImage(member.getMember()), 152, 174, null);

            // Member's name and discriminator
            String name = member.getMember().getUser().getName().length() < 18 ? member.getMember().getUser().getName() : member.getMember().getUser().getName().substring(0, 18);
            String discriminator = "#" + member.getMember().getUser().getDiscriminator();

            // Draw name and discriminator to card
            g.setFont(new Font("Ubuntu", Font.PLAIN, 30));
            g.setColor(Color.WHITE);
            g.drawString(name, picture.getWidth() / 4, 150);
            g.setColor(Constants.DEFAULT_DISCORD_COLOR_LIGHT);
            g.drawString(discriminator, picture.getWidth() / 4 + g.getFontMetrics().stringWidth(name), 150);

            // Init space width sizes
            int rank_size = 10;
            int text_size = 10;
            int level_size = 10;

            // Draw Rank and Level to card
            g.setColor(Color.WHITE);
            g.setFont(g.getFont().deriveFont(20.0F));
            g.drawString("RANK", picture.getWidth() / 2, 60);
            text_size += g.getFontMetrics().stringWidth("RANK");
            g.setFont(g.getFont().deriveFont(60.0F));
            g.drawString("#" + guild.getMembersPosition(member), picture.getWidth() / 2 + text_size, 60);
            rank_size += g.getFontMetrics().stringWidth("#" + guild.getMembersPosition(member));

            g.setColor(Constants.DEFAULT_BLUE_COLOR);
            g.setFont(g.getFont().deriveFont(20.0F));
            g.drawString("LEVEL", picture.getWidth() / 2 + text_size + rank_size, 60);
            level_size += g.getFontMetrics().stringWidth("LEVEL");
            g.setFont(g.getFont().deriveFont(60.0F));
            g.drawString(String.valueOf(member.getLeveling().getLevel()), picture.getWidth() / 2 + text_size + level_size + rank_size, 60);

            // Draw XP to card
            g.setColor(Color.WHITE);
            g.setFont(new Font("Ubuntu", Font.PLAIN, 20));
            g.drawString(String.valueOf(member.getLeveling().getExperience()), Utils.getValueFromPercent(picture.getWidth(), 75) - Utils.getValueFromPercent(picture.getWidth(), 15), 250);
            g.setColor(Constants.DEFAULT_DISCORD_COLOR_LIGHT);
            g.drawString(" / " + member.getLeveling().getNeededExperiences() + " XP", g.getFontMetrics().stringWidth(String.valueOf(member.getLeveling().getExperience())) + Utils.getValueFromPercent(picture.getWidth(), 75) - Utils.getValueFromPercent(picture.getWidth(), 15), 250);
            // Generate card
            g.dispose();

            // Save card to file
            File file = File.createTempFile(member.getMember().getUser().getId() + "-" + member.getMember().getGuild().getId(), ".png");

            ImageIO.write(picture, "png", file);
            return file;
        } catch (IOException e) {
            PuffinLogger.fatalMessage("An error has occurred while generating a card.");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onCommand(Member member, @NotNull Message message, TextChannel channel, String[] args, EventWaiter waiter, PGuild guild) {
        final File image;

        if(message.getMentionedMembers().size() > 0) {
            if(message.getMentionedMembers().get(0).getUser().isBot()) {
                channel.sendMessage(Constants.RED_DENY + " " + Translator.translate(this, "messages.member_is_bot", guild, message.getMentionedMembers().get(0).getUser().getName())).queue();
                return;
            }
            image = getRankCard(guild.getMemberById(message.getMentionedMembers().get(0).getId()));
            channel.sendFile(image).queue(a -> image.delete());
            return;
        }

        image = getRankCard(guild.getMemberById(member.getId()));
        channel.sendFile(image).queue(a -> image.delete());
    }

    @Override
    public String getName() {
        return "rank";
    }

    @Override
    public CommandType getType() {
        return CommandType.GENERAL;
    }

    @Override
    public Permission[] userPermission() {
        return new Permission[0];
    }

    @Override
    public Permission[] botPermission() {
        return new Permission[0];
    }

    @Override
    public String[] getAliases() {
        return new String[]{"level"};
    }
}
