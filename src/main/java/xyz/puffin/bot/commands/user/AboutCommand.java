package xyz.puffin.bot.commands.user;

import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import xyz.puffin.bot.Main;
import xyz.puffin.bot.commands.Command;
import xyz.puffin.bot.commands.CommandType;
import xyz.puffin.bot.objects.PGuild;
import xyz.puffin.bot.utils.Constants;
import xyz.puffin.bot.utils.Translator;

public class AboutCommand implements Command {

    @Override
    public void onCommand(Member member, Message message, TextChannel channel, String[] args, EventWaiter waiter, PGuild guild) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setColor(Constants.DEFAULT_BLUE_COLOR);
        builder.setThumbnail(Main.getJda().getSelfUser().getAvatarUrl());
        builder.setTitle(Translator.translate(this, "messages.title", guild));
        builder.setDescription(Translator.translate(this, "messages.description", guild));
        channel.sendMessage(builder.build()).queue();
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public CommandType getType() {
        return CommandType.GENERAL;
    }

    @Override
    public Permission[] userPermission() {
        return new Permission[0];
    }

    @Override
    public Permission[] botPermission() {
        return new Permission[0];
    }
}
