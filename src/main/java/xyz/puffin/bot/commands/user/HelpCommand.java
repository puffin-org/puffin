package xyz.puffin.bot.commands.user;

import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import xyz.puffin.bot.Main;
import xyz.puffin.bot.commands.Command;
import xyz.puffin.bot.commands.CommandType;
import xyz.puffin.bot.objects.PGuild;
import xyz.puffin.bot.utils.Constants;
import xyz.puffin.bot.utils.Translator;

public class HelpCommand implements Command {

    @Override
    public void onCommand(Member member, Message message, TextChannel channel, String[] args, EventWaiter waiter, PGuild guild) {
        channel.sendMessage(new EmbedBuilder().setColor(Constants.DEFAULT_BLUE_COLOR).setTitle(Translator.translate(this, "messages.title", guild)).setDescription(Translator.translate(this, "messages.description", guild)).build()).queue();
        member.getUser().openPrivateChannel().queue(ch -> {
            EmbedBuilder builder = new EmbedBuilder();
            builder.setColor(Constants.DEFAULT_BLUE_COLOR);
            builder.setTitle(Translator.translate(this, "messages.help", guild));
            StringBuilder sb = new StringBuilder();
            for(CommandType type : CommandType.getTypes()) {
                if(type.equals(CommandType.STAFF) && !Main.getInstance().getConfig().getArray("discord.staff").contains(member.getId())) {
                    continue;
                }

                sb.append(type.getEmote() + " | **" + Translator.translate("commands.types." + type.name().toLowerCase(), guild) + "** - " + Main.getInstance().getCommandHandler().getCommandsByType(type).size()).append("\n");
                for(Command command : Main.getInstance().getCommandHandler().getCommandsByType(type)) {
                    if(command.getName() != null) {
                        sb.append("``" + command.getName() + "``").append(" ");
                    }
                }
                sb.append("\n\n");
            }
            builder.setDescription(Translator.translate(this, "messages.help_description", guild, guild.getGuild().getName(), guild.getConfiguration().getPrefix()) + "\n\n" + sb.toString());
            ch.sendMessage(builder.build()).queue();
        });
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public CommandType getType() {
        return CommandType.GENERAL;
    }

    @Override
    public Permission[] userPermission() {
        return new Permission[0];
    }

    @Override
    public Permission[] botPermission() {
        return new Permission[0];
    }
}
