package xyz.puffin.bot.commands.admin;

import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import xyz.puffin.bot.commands.Command;
import xyz.puffin.bot.commands.CommandType;
import xyz.puffin.bot.objects.PGuild;
import xyz.puffin.bot.utils.MessageUtils;

public class TemplateCommand implements Command {

    @Override
    public void onCommand(Member member, Message message, TextChannel channel, String[] args, EventWaiter waiter, PGuild guild) {
        if(args.length < 1) {
            channel.sendMessage(MessageUtils.getHelpMessage(this, guild));
            return;
        }
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public CommandType getType() {
        return CommandType.ADMINISTRATION;
    }

    @Override
    public Permission[] userPermission() {
        return new Permission[]{Permission.ADMINISTRATOR};
    }

    @Override
    public Permission[] botPermission() {
        return new Permission[]{Permission.ADMINISTRATOR};
    }
}
