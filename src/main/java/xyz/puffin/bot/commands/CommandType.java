package xyz.puffin.bot.commands;

public enum CommandType {

    // General command
    GENERAL("\uD83D\uDCCB"),

    // Command for moderation
    MODERATION("\uD83D\uDEE0"),

    // Command for administration
    ADMINISTRATION("\uD83C\uDFB4"),

    // Command for managing bot
    STAFF("\uD83D\uDC8E");

    // Type's emote
    private String emote;

    CommandType(String emote) {
        this.emote = emote;
    }

    /**
     * Returns all command types
     *
     * @return all command types
     */
    public static CommandType[] getTypes() {
        return new CommandType[]{GENERAL, MODERATION, ADMINISTRATION, STAFF};
    }

    /**
     * Returns type's emote
     *
     * @return type's emote
     */
    public String getEmote() {
        return emote;
    }
}
