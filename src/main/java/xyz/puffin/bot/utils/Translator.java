package xyz.puffin.bot.utils;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import xyz.puffin.bot.commands.Command;
import xyz.puffin.bot.objects.PGuild;

public class Translator {

    /**
     * Translates message using json path
     *
     * @param path  json path
     * @param guild guild where is puffin
     * @return translated text
     */
    public static String translate(String path, @NotNull PGuild guild) {
        return guild.getConfiguration().getLanguage().getString(path);
    }


    /**
     * Translates command message using json path
     *
     * @param command puffin's command
     * @param path    json path
     * @param guild   guild where is puffin
     * @return translated text
     */
    public static String translate(@NotNull Command command, String path, PGuild guild) {
        return translate("commands." + command.getName() + "." + path, guild);
    }

    /**
     * Translates message with arguments using json path
     *
     * @param path  json path
     * @param guild guild where is puffin
     * @param args  {@link String} {@link Integer} {@link Character} {@link Long} {@link Double} {@link Float} {@link Boolean} arguments in translation
     * @return translated text
     */
    public static String translate(String path, PGuild guild, Object... args) {
        if(!isValid(args)) {
            PuffinLogger.dangerMessage("Error while translating: Args can't be objects or null");
            return "";
        }

        return String.format(translate(path, guild), args);
    }

    /**
     * Translates command message with arguments using json path
     *
     * @param command puffin's command
     * @param path    json path
     * @param guild   guild where is puffin
     * @param args    {@link String} {@link Integer} {@link Character} {@link Long} {@link Double} {@link Float} {@link Boolean} arguments in translation
     * @return translated text
     */
    public static String translate(Command command, String path, PGuild guild, Object... args) {
        if(!isValid(args)) {
            return String.format(translate(command, path, guild), args.toString());
        }
        return String.format(translate(command, path, guild), args);
    }

    /**
     * Checks if arguments are valid
     *
     * @param args arguments
     * @return validity of arguments
     */
    @Contract(pure = true)
    private static boolean isValid(@NotNull Object... args) {
        for(Object object : args) {
            if(object == null) {
                return false;
            }
            if(!(object instanceof String) && !(object instanceof Integer) && !(object instanceof Character) && !(object instanceof Long) && !(object instanceof Double) && !(object instanceof Float) && !(object instanceof Boolean)) {
                return false;
            }
        }
        return true;
    }
}
