package xyz.puffin.bot.utils;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import net.dv8tion.jda.api.entities.Member;
import xyz.puffin.bot.Main;

import javax.annotation.Nullable;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

public class Utils {

    /**
     * Returns haste content
     *
     * @param url url to haste
     * @return text from haste
     */
    @Nullable
    public static String copyHaste(String url) {
        try {
            return Unirest.get(url)
                    .header("User-Agent", "Mozilla/5.0 FlareBot")
                    .header("Content-Type", "text/plain")
                    .asJson().getBody().toString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Uploads content to hastebin
     *
     * @param trace message to paste
     * @return url to haste
     */
    @Nullable
    public static String createHaste(String trace, boolean raw) {
        try {
            String url = raw ? "https://paste.imspeedy.eu/raw/" : "https://paste.imspeedy.eu/";
            return url + Unirest.post("https://paste.imspeedy.eu/documents")
                    .header("User-Agent", "Mozilla/5.0 FlareBot")
                    .header("Content-Type", "text/plain")
                    .body(trace)
                    .asJson()
                    .getBody()
                    .getObject().getString("key");
        } catch (UnirestException e) {
            return null;
        }
    }

    /**
     * Returns member's avatar
     *
     * @param member discord's member
     * @return avatar image
     */
    @Nullable
    public static BufferedImage getAvatar(Member member) {
        // Getting member's avatar from discord api
        try {
            URLConnection connection = new URL(member.getUser().getAvatarUrl() + "?size=1024").openConnection();
            connection.addRequestProperty("User-Agent", "Mozilla/5.0");
            return ImageIO.read(connection.getInputStream());
        } catch (IOException ex) {
            PuffinLogger.fatalMessage("Error while reading members's default avatar image");
            ex.printStackTrace();
        }

        // Getting default member's avatar from discord api
        try {
            URLConnection connection = new URL(member.getUser().getDefaultAvatarUrl() + "?size=1024").openConnection();
            connection.addRequestProperty("User-Agent", "Mozilla/5.0");
            return ImageIO.read(connection.getInputStream());
        } catch (IOException ex) {
            PuffinLogger.fatalMessage("Error while reading discord's default avatar image");
            ex.printStackTrace();
        }

        // Getting default avatar from discord api
        try {
            return ImageIO.read(Main.class.getResourceAsStream("/default-avatar.png"));
        } catch (IOException ex) {
            PuffinLogger.fatalMessage("Error while reading default avatar image");
            ex.printStackTrace();
        }
        return null;
    }

    public static float getPercent(long value, long max) {
        return ((value * 100.0f) / max);
    }

    public static float getValueFromPercent(int value, int percent) {
        return (value / 100) * percent;
    }
}
