package xyz.puffin.bot.utils;

import xyz.puffin.bot.Main;

public class PuffinLogger {

    /**
     * Sends great message
     *
     * @param text message
     */
    public static void greatMessage(final String text) {
        Main.LOGGER.info(AnsiColor.GREEN.applyTo("✔ success") + "  " + text);
    }

    /**
     * Sends warn message
     *
     * @param text message
     */
    public static void warnMessage(final String text) {
        Main.LOGGER.info(AnsiColor.YELLOW.applyTo("⚠ warn") + "     " + text);
    }

    /**
     * Sends danger message
     *
     * @param text message
     */
    public static void dangerMessage(final String text) {
        Main.LOGGER.info(AnsiColor.RED.applyTo("✖ danger") + "   " + text);
    }

    /**
     * Sends fatal error message
     *
     * @param text message
     */
    public static void fatalMessage(final String text) {
        Main.LOGGER.info(AnsiColor.RED.applyTo("✖ fatal") + "    " + text);
    }

    /**
     * Sends info message
     *
     * @param text message
     */
    public static void infoMessage(final String text) {
        Main.LOGGER.info(AnsiColor.BLUE.applyTo("ℹ info") + "       " + text);
    }

    /**
     * Sends debug message
     *
     * @param text message
     */
    public static void debugMessage(final String text) {
        Main.LOGGER.info(AnsiColor.MAGENTA.applyTo("… debug") + "    " + text);
    }

    /**
     * Sends command execution message
     *
     * @param text message
     */
    public static void commandMessage(final String text) {
        Main.LOGGER.info(AnsiColor.CYAN.applyTo("❯ command") + "  " + text);
    }

}
