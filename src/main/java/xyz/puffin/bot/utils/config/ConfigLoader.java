package xyz.puffin.bot.utils.config;

import com.afollestad.ason.Ason;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.text.translate.UnicodeUnescaper;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import xyz.puffin.bot.Main;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class ConfigLoader {

    /**
     * This will attempt to load the config and create it if it is not there
     *
     * @param file the file to load
     * @return the loaded config
     * @throws Exception if something goes wrong
     */
    @NotNull
    @Contract("_ -> new")
    public static Config getConfig(@NotNull final File file) throws Exception {
        if(!file.exists()) {
            file.createNewFile();
            final FileWriter writer = new FileWriter(file);
            JsonObject object = new JsonParser().parse(new FileReader(Main.class.getResource("/" + file.getPath()).getFile())).getAsJsonObject();
            writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(object));
            writer.close();
        }
        return new MainConfig(file);
    }

    @NotNull
    @Contract("_ -> new")
    public static Config getLanguage(@NotNull final File file) throws Exception {
        if(!file.exists()) {
            file.createNewFile();
            final FileWriter writer = new FileWriter(file);
            JsonObject object = new JsonParser().parse(new FileReader(Main.class.getResource("/languages/" + file.getName()).getFile())).getAsJsonObject();
            writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(object));
            writer.close();
        }
        return new MainConfig(file);
    }

    public static class MainConfig extends Config {

        private final File configFile;

        MainConfig(final File file) throws Exception {
            super(null, new Ason(Files.asCharSource(file, Charsets.UTF_8).read()));
            this.configFile = file;
        }

        @Override
        public File getConfigFile() {
            return this.configFile;
        }

        @Override
        public void save() throws Exception {
            try {
                final BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(new FileOutputStream(this.configFile), StandardCharsets.UTF_8));
                new UnicodeUnescaper().translate(
                        this.config.toString(4), writer);
                writer.close();
            } catch (final IOException e) {
                e.printStackTrace();
                throw e;
            }
        }
    }
}