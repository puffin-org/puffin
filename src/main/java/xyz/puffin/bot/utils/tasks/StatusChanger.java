package xyz.puffin.bot.utils.tasks;

import net.dv8tion.jda.api.entities.Activity;
import xyz.puffin.bot.Main;

import java.util.TimerTask;

public class StatusChanger extends TimerTask {

    private int i = 0;
    @Override
    public void run() {
        if(i == 0) {
            Main.getJda().getPresence().setActivity(Activity.listening(Main.getJda().getGuilds().size() + " guilds"));
            i++;
        } else if(i == 1) {
            Main.getJda().getPresence().setActivity(Activity.watching(Main.getJda().getUsers().size() + " users"));
            i++;
        } else if (i == 2) {
            Main.getJda().getPresence().setActivity(Activity.streaming("Created by Speedy11CZ", "https://twitch.tv/speedy11cz"));
            i++;
        } else {
            Main.getJda().getPresence().setActivity(Activity.watching("https://puffinbot.xyz/"));
            i = 0;
        }
    }
}
