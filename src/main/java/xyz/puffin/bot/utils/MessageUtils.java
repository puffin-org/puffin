package xyz.puffin.bot.utils;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import xyz.puffin.bot.commands.Command;
import xyz.puffin.bot.objects.PGuild;

import java.awt.*;

public class MessageUtils {

    /**
     * Returns message with help
     *
     * @param command puffin's command
     * @param guild   guild where is puffin
     * @return help message
     */
    public static MessageEmbed getHelpMessage(Command command, PGuild guild) {
        return new EmbedBuilder().setColor(Color.RED).setTitle(Translator.translate("error.args.title", guild), Translator.translate("error.args.description", guild, "commands." + command.getName() + ".help")).build();
    }
}
