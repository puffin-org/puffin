package xyz.puffin.bot.utils.graphics;

import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Member;
import xyz.puffin.bot.utils.Constants;
import xyz.puffin.bot.utils.Utils;

import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;

public class GraphicsUtils {

    /**
     * Updates image background
     *
     * @param g   graphics
     * @param img card's image
     */
    public static void setBackground(Graphics g, BufferedImage img) {
        g.setColor(Constants.DEFAULT_DISCORD_COLOR);
        g.fillRect(0, 0, img.getWidth(), img.getHeight());
    }

    /**
     * Draws progress bar to image
     *
     * @param g       graphics
     * @param value   value
     * @param maximum maximum value
     * @param gp1     gradient color
     * @param img     card's image
     */
    public static void drawProgressBar(Graphics2D g, long value, long maximum, GradientPaint gp1, BufferedImage img) {
        int drawAmount = (int) ((Utils.getPercent(value, maximum) / 100) * (Utils.getValueFromPercent(img.getWidth(), 75)));
        g.setColor(Constants.DEFAULT_DISCORD_COLOR_LIGHT);
        g.fill(new RoundRectangle2D.Float(Utils.getValueFromPercent(img.getWidth(), 25), 170, Utils.getValueFromPercent(img.getWidth(), 75), 50, 50, 50));

        g.setPaint(gp1);
        g.fill(new RoundRectangle2D.Float(Utils.getValueFromPercent(img.getWidth(), 25), 170, drawAmount, 50, 50, 50));
    }

    /**
     * Returns member's avatar
     *
     * @param member discord's member
     * @return
     */
    public static BufferedImage getStatusImage(Member member) {
        BufferedImage output = new BufferedImage(46, 46, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = output.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Constants.DEFAULT_DISCORD_COLOR);
        g2.fill(new RoundRectangle2D.Float(0, 0, 46, 46, 100, 100));
        if(member.getActivities().size() > 0 && member.getActivities().get(0).getType() == Activity.ActivityType.STREAMING) {
            g2.setColor(Constants.STATUS_STREAMING_COLOR);
        } else {
            switch (member.getOnlineStatus()) {
                case ONLINE:
                    g2.setColor(Constants.STATUS_ONLINE_COLOR);
                    break;
                case IDLE:
                    g2.setColor(Constants.STATUS_IDLE_COLOR);
                    break;
                case DO_NOT_DISTURB:
                    g2.setColor(Constants.STATUS_DND_COLOR);
                    break;
                default:
                    g2.setColor(Constants.STATUS_OFFLINE_COLOR);
                    break;
            }
        }
        g2.fill(new RoundRectangle2D.Float(3, 3, 40, 40, 100, 100));
        g2.dispose();
        return output;
    }

    /**
     * Returns rounded image
     *
     * @param image        card's image
     * @param cornerRadius radius
     * @return cornered image
     */
    public static BufferedImage makeRoundedCorner(BufferedImage image, int cornerRadius) {
        int w = image.getWidth();
        int h = image.getHeight();
        BufferedImage output = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2 = output.createGraphics();
        g2.setComposite(AlphaComposite.Src);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.WHITE);
        g2.fill(new RoundRectangle2D.Float(0, 0, w, h, cornerRadius, cornerRadius));
        g2.setComposite(AlphaComposite.SrcIn);
        g2.drawImage(image, 0, 0, null);

        g2.dispose();

        return output;
    }
}
