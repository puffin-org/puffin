package xyz.puffin.bot.utils;

import java.awt.*;

public class Constants {

    // Default discord color pallet
    public static final Color DEFAULT_DISCORD_COLOR = Color.decode("#23272A");
    public static final Color DEFAULT_DISCORD_COLOR_LIGHT = Color.decode("#919191");

    // Default bot color pallet
    public static final Color DEFAULT_BLUE_COLOR = Color.decode("#129BCA");
    public static final Color DEFAULT_GREEN_COLOR = Color.decode("#6CAF82");

    // Status colors
    public static final Color STATUS_ONLINE_COLOR = Color.decode("#76e66c");
    public static final Color STATUS_IDLE_COLOR = Color.decode("#ffc757");
    public static final Color STATUS_DND_COLOR = Color.decode("#f54747");
    public static final Color STATUS_STREAMING_COLOR = Color.decode("#a943f7");
    public static final Color STATUS_OFFLINE_COLOR = Color.decode("#919191");

    // Emotes
    public static final String ANIMATED_LOADING_EMOTE = "<a:loading:601109637282005010>";

    // Marks
    final public static String GREEN_OK = "\u2705";
    final public static String RED_DENY = "\u26D4";

    // Emotes
    final public static String HOMES = "\uD83C\uDFD8";
    final public static String USERS = "\uD83D\uDC65";
    final public static String PENCIL = "\uD83D\uDCDD";
    final public static String MEGAFON = "\uD83D\uDCE3";
    final public static String JDA = "<:jda:622780846037925908>";
    final public static String COMPRESS = "\uD83D\uDDDC";
    final public static String FLOPY_DISC = "\uD83D\uDCBE";
    final public static String COMET = "\u2604";
    final public static String JAVA = "<:java:622780846654619648>";
}
