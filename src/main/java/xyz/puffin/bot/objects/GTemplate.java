package xyz.puffin.bot.objects;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildChannel;
import net.dv8tion.jda.api.entities.PermissionOverride;
import net.dv8tion.jda.api.entities.Role;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import xyz.puffin.bot.Main;
import xyz.puffin.bot.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class GTemplate {

    private List<TChannel> channels = new ArrayList<>();
    private List<TRole> roles = new ArrayList<>();

    public GTemplate(String id) {
        Guild guild = Main.getJda().getGuildById(id);
        for(Role role : guild.getRoles()) {
            if(role.isManaged()) continue;
            roles.add(new TRole(role));
        }

        for(GuildChannel channel : guild.getChannels()) {
            channels.add(new TChannel(channel));
        }
    }

    @NotNull
    public static String upload(String json) {
        return Utils.createHaste(json, true).substring(30);
    }

    @NotNull
    public static String download(String code) {
        return Utils.copyHaste("https://paste.imspeey.eu/" + code).substring(30);
    }

    public List<TChannel> getChannels() {
        return channels;
    }

    public List<TRole> getRoles() {
        return roles;
    }

    private class TRole {
        private String name;
        private boolean mentionable;
        private boolean hoisted;
        private String color;
        private long permissions;

        public TRole(@NotNull Role role) {
            String hexColour = "00000";
            if(role.getColor() != null) {
                hexColour = Integer.toHexString(role.getColor().getRGB() & 0xffffff);
                if(hexColour.length() < 6) {
                    hexColour = "000000".substring(0, 6 - hexColour.length()) + hexColour;
                }
            }

            this.name = role.getName();
            this.mentionable = role.isMentionable();
            this.hoisted = role.isHoisted();
            this.color = "#" + hexColour;
            this.permissions = role.getPermissionsRaw();
        }

        public String getName() {
            return name;
        }

        public boolean isMentionable() {
            return mentionable;
        }

        public boolean isHoisted() {
            return hoisted;
        }

        public String getColor() {
            return color;
        }

        public long getPermissions() {
            return permissions;
        }
    }

    private class TChannel {
        private String name;
        private String type;
        private String category;
        private List<BCPerm> permissions = new ArrayList<>();

        public TChannel(@NotNull GuildChannel channel) {
            this.name = channel.getName();
            this.type = channel.getType().toString();
            this.category = channel.getParent() != null ? channel.getParent().getName() : "";
            for(PermissionOverride permissionOverride : channel.getPermissionOverrides()) {
                if(permissionOverride.isRoleOverride()) {
                    permissions.add(new BCPerm(permissionOverride.getRole().getName(), permissionOverride.getAllowedRaw(), permissionOverride.getDeniedRaw()));
                }
            }
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        public String getCategory() {
            return category;
        }

        public List<BCPerm> getPermissions() {
            return permissions;
        }

        private class BCPerm {
            private String name;
            private long allowed;
            private long denied;

            @Contract(pure = true)
            public BCPerm(String name, long allowed, long denied) {
                this.name = name;
                this.allowed = allowed;
                this.denied = denied;
            }

            public String getName() {
                return name;
            }

            public long getAllowed() {
                return allowed;
            }

            public long getDenied() {
                return denied;
            }
        }
    }
}
