package xyz.puffin.bot.objects;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import xyz.puffin.bot.Main;
import xyz.puffin.bot.managers.GuildManager;
import xyz.puffin.bot.utils.Translator;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.Random;

public class GMember {

    // Member's guild database id
    private String database_id;

    // Member's guild id
    private String guild_id;

    // Member's leveling
    private Leveling leveling;

    @Contract(pure = true)
    public GMember(final String id) {
        this.database_id = id;
        this.leveling = new Leveling();
    }

    /**
     * Returns guild's database id
     *
     * @return database id
     */
    public String getDatabaseId() {
        return database_id;
    }

    /**
     * Returns guild's id
     *
     * @return guild id
     */
    public String getGuildId() {
        return guild_id;
    }

    /**
     * Updates guild's id
     *
     * @param guild_id guild's id
     * @return self instance
     */
    public GMember setGuildId(final String guild_id) {
        this.guild_id = guild_id;
        return this;
    }

    /**
     * Returns member's guild
     *
     * @return guild where is puffin
     */
    public PGuild getGuild() {
        return GuildManager.getGuildById(guild_id);
    }

    /**
     * Returns discord's member
     *
     * @return discord member
     */
    public Member getMember() {
        if(getGuild() == null) return null;
        return getGuild().getGuild().getMemberById(database_id);
    }

    /**
     * Returns member's leveling
     *
     * @return member's leveling
     */
    public Leveling getLeveling() {
        return leveling;
    }

    // Leveling class
    public class Leveling {

        // Member's experience
        private long experience;

        // Member's level
        private int level;

        // Members's last message
        private OffsetDateTime last_message;

        @Contract(pure = true)
        public Leveling() {
            this.experience = 0;
            this.level = 0;
            this.last_message = OffsetDateTime.MIN;
        }

        /**
         * Returns member's level
         *
         * @return member's level
         */
        public int getLevel() {
            return level;
        }

        /**
         * Updates member's level
         *
         * @param level new level
         * @return self instance
         */
        public Leveling setLevel(@NotNull int level) {
            this.level = level;
            return this;
        }

        /**
         * Returns member's experience
         *
         * @return member's experience
         */
        public long getExperience() {
            return experience;
        }

        /**
         * Updates member's experience
         *
         * @param experience new experience
         * @return self instance
         */
        public Leveling setExperience(@NotNull long experience) {
            this.experience = experience;
            return this;
        }

        /**
         * Returns needed experiences for rank up
         *
         * @return amount of needed experiences
         */
        public int getNeededExperiences() {
            return 5 * (level ^ 2) + 50 * level + 100;
        }

        /**
         * Returns true when member's can gain xp
         *
         * @param new_message new message
         * @return can receive level
         */
        public boolean canGainXP(OffsetDateTime new_message) {
            return Duration.between(last_message, new_message).getSeconds() > 60;
        }

        /**
         * Returns last rewarded message
         *
         * @return last rewared message
         */
        public OffsetDateTime getLastMessage() {
            return last_message;
        }

        /**
         * Updates last rewarded message
         *
         * @param last_message last rewarded message
         * @return self instance
         */
        public Leveling updateLastMessage(OffsetDateTime last_message) {
            this.last_message = last_message;
            return this;
        }

        /**
         * Updates member's leveling
         *
         * @param channel text channel
         * @return self instance
         */
        public Leveling updateLeveling(TextChannel channel) {
            int xpAmount = new Random().nextInt(10) + 21;


            experience += xpAmount;
            if(getNeededExperiences() <= experience) {
                experience -= getNeededExperiences();
                level++;
                if(getGuild().getGuild().getSelfMember().hasPermission(Permission.MESSAGE_WRITE)) channel.sendMessage(Translator.translate("messages.leveling.rankup_message", getGuild(), getMember().getAsMention(), level)).queue();
            }
            Main.getInstance().getSql().updateMembersLeveling(GuildManager.getGuildById(guild_id).getDatabaseId(), getMember().getUser().getId(), level, experience);
            return this;
        }
    }
}
