package xyz.puffin.bot.objects;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import org.jetbrains.annotations.NotNull;
import xyz.puffin.bot.Main;
import xyz.puffin.bot.managers.LanguageManager;
import xyz.puffin.bot.utils.config.Config;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class PGuild {

    // Guild's database id
    private long database_id;

    // Guild's id
    private String guild_id;

    // Guild's members
    private List<GMember> members = new ArrayList<>();

    // Guild's configuration
    private Configuration configuration;

    /**
     * Creates guild's instance
     *
     * @param database_id guild's database id
     * @param guild_id    guild's id
     */
    public PGuild(final long database_id, final String guild_id) {
        this.database_id = database_id;
        this.guild_id = guild_id;
        this.configuration = new Configuration();
    }

    /**
     * Returns guild's database id
     *
     * @return guild's database id
     */
    public long getDatabaseId() {
        return database_id;
    }

    /**
     * Returns guild's id
     *
     * @return guild's id
     */
    public String getGuildId() {
        return guild_id;
    }

    /**
     * Returns guild where is puffin
     *
     * @return guild where is puffin
     */
    public Guild getGuild() {
        return Main.getJda().getGuildById(guild_id);
    }


    /**
     * Returns list of guild's members
     *
     * @return list of guild's members
     */
    public List<GMember> getMembers() {
        return members;
    }

    /**
     * Initialize member's data in guild
     *
     * @return self instance
     */
    public PGuild initMembers() {
        HashMap<String, GMember> membersMap = new HashMap<>();

        Main.getInstance().getSql().getMembers(database_id).forEach(member -> membersMap.put(member.getDatabaseId(), member));

        for(Member member : getGuild().getMembers()) {
            if(!membersMap.containsKey(member.getId())) {
                members.add(Main.getInstance().getSql().createMember(database_id, member.getId()).setGuildId(guild_id));
            } else {
                members.add(membersMap.get(member.getId()).setGuildId(guild_id));
            }
        }
        return this;
    }

    /**
     * Adds member to cache and create column in database when not exist
     *
     * @param member guild's member
     * @return self instance
     */
    public PGuild memberJoin(@NotNull final Member member) {
        if(this.getMemberById(member.getId()) != null) {
            return this;
        }

        if(!Main.getInstance().getSql().existsMemberData(database_id, member.getId())) {
            members.add(Main.getInstance().getSql().createMember(database_id, member.getId()));
            return this;
        }
        members.add(Main.getInstance().getSql().getMember(database_id, member.getId()));
        return this;
    }

    /**
     * Removes member from cache
     *
     * @param member guild's member
     * @return self instance
     */
    public PGuild memberLeave(@NotNull final Member member) {
        if(this.getMemberById(member.getId()) == null) {
            return this;
        }
        members.remove(this.getMemberById(member.getId()));
        return this;
    }

    /**
     * Returns guild's member by id
     *
     * @param member_id member's id
     * @return member's data
     */
    public GMember getMemberById(String member_id) {
        for(GMember member : members) {
            if(member.getMember() != null) {
                if(member.getMember().getId().equals(member_id)) {
                    return member;
                }
            }
        }
        return null;
    }

    /**
     * Returns member's leveling position
     *
     * @param member guild's member
     * @return leveling position
     */
    public int getMembersPosition(GMember member) {
        List<GMember.Leveling> sorted = new ArrayList<>();
        members.forEach(m -> sorted.add(m.getLeveling()));

        sorted.sort(Comparator.comparingInt(GMember.Leveling::getLevel).thenComparing(GMember.Leveling::getExperience).reversed());
        return sorted.indexOf(member) + 1;
    }

    /**
     * Returns guild's configuration
     *
     * @return guild's configuration
     */
    public Configuration getConfiguration() {
        return configuration;
    }

    public class Configuration {

        // Guild's prefix
        private String prefix;

        // Guild's reward roles
        public List<String> reward_roles = new ArrayList<>();

        // Guild's language
        private String language;

        public Configuration() {
            this.prefix = "%";
            this.language = "en";
        }

        /**
         * Returns guild's prefix
         *
         * @return guild's prefix
         */
        public String getPrefix() {
            return prefix;
        }

        /**
         * Updates guild's prefix
         *
         * @param prefix new prefix
         * @return self instance
         */
        public Configuration setPrefix(String prefix) {
            this.prefix = prefix;
            return this;
        }

        /**
         * Returns guild's reward roles ids
         *
         * @return reward roles ids
         */
        public List<String> getRewardRoles() {
            return reward_roles;
        }

        /**
         * Adds guild's reward role
         *
         * @param roleId role's id
         * @return self instance
         */
        public Configuration addRewardRole(String roleId) {
            this.reward_roles.add(roleId);
            return this;
        }

        /**
         * Removes guild's reward role
         *
         * @param roleId role's id
         * @return self instance
         */
        public Configuration removeRewardRole(String roleId) {
            this.reward_roles.remove(roleId);
            return this;
        }

        /**
         * Returns guild's language
         *
         * @return guild's language
         */
        public Config getLanguage() {
            return LanguageManager.getLanguageById(language);
        }

        /**
         * Updates guild's language
         *
         * @param language new language
         * @return self instance
         */
        public Configuration setLanguage(final String language) {
            this.language = language;
            return this;
        }

    }
}
