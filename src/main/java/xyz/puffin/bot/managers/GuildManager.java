package xyz.puffin.bot.managers;

import net.dv8tion.jda.api.entities.Guild;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import xyz.puffin.bot.Main;
import xyz.puffin.bot.objects.PGuild;
import xyz.puffin.bot.utils.PuffinLogger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GuildManager {

    private static List<PGuild> guilds = new ArrayList<>();

    public static void loadGuilds() {
        PuffinLogger.infoMessage("Loading all guilds");
        HashMap<String, PGuild> guildsMap = new HashMap<>();
        Main.getInstance().getSql().getGuilds().forEach(guild -> guildsMap.put(guild.getGuildId(), guild));

        for(Guild guild : Main.getJda().getGuilds()) {
            if(!Main.getInstance().getSql().existsGuildData(guild.getId())) {
                PuffinLogger.infoMessage("Guild with id " + guild.getId() + " don't exist, registering in database...");
                guilds.add(Main.getInstance().getSql().createGuild(guild.getId()).initMembers());
            } else {
                guilds.add(guildsMap.get(guild.getId()).initMembers());
            }
        }
        PuffinLogger.greatMessage("Successfully loaded " + guilds.size() + " guilds");
    }

    @Contract(pure = true)
    public static List<PGuild> getGuilds() {
        return guilds;
    }

    @Nullable
    public static PGuild getGuildById(String id) {
        for(PGuild guild : guilds) {
            if(guild.getGuildId().equals(id)) {
                return guild;
            }
        }
        return null;
    }


    /**
     * Adds guild to cache and create column in database when not exist
     *
     * @param guild guild where is puffin
     */
    public static void botJoin(@NotNull final Guild guild) {
        if(getGuildById(guild.getId()) != null) {
            return;
        }

        if(!Main.getInstance().getSql().existsGuildData(guild.getId())) {
            guilds.add(Main.getInstance().getSql().createGuild(guild.getId()).initMembers());
            return;
        }
        guilds.add(Main.getInstance().getSql().getGuild(guild.getId()).initMembers());
        return;
    }

    /**
     * Removes guild from cache and database
     *
     * @param guild guild where is puffin
     */
    public static void botLeave(@NotNull final Guild guild) {
        if(getGuildById(guild.getId()) == null) {
            return;
        }
        guilds.remove(getGuildById(guild.getId()));
        Main.getInstance().getSql().deleteGuild(guild.getId());
        return;
    }
}
