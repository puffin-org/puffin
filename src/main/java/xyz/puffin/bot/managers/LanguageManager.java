package xyz.puffin.bot.managers;

import com.afollestad.ason.Ason;
import org.jetbrains.annotations.Contract;
import xyz.puffin.bot.Main;
import xyz.puffin.bot.utils.PuffinLogger;
import xyz.puffin.bot.utils.config.Config;
import xyz.puffin.bot.utils.config.ConfigLoader;

import java.io.File;
import java.util.HashMap;
import java.util.List;

public class LanguageManager {

    private static HashMap<String, Config> languages = new HashMap<>();

    /**
     * Loads all languages registered in config
     */
    public static void loadLanguages() {
        PuffinLogger.infoMessage("Loading all languages");

        if(!new File("languages").exists()) {
            new File("languages").mkdir();
        }

        List<Ason> langs = Main.getInstance().getConfig().getArray("languages");
        for(Ason lang : langs) {
            try {
                languages.put(lang.getString("id"), ConfigLoader.getLanguage(new File("languages/" + lang.getString("id") + ".json")));
            } catch (Exception e) {
                PuffinLogger.fatalMessage("Error while loading language file");
                e.printStackTrace();
            }
        }
        PuffinLogger.greatMessage("Successfully loaded " + languages.size() + " languages");
    }

    /**
     * Returns all languages
     *
     * @return hash map of all languages
     */
    @Contract(pure = true)
    public static HashMap<String, Config> getLanguages() {
        return languages;
    }

    /**
     * Returns language by id
     *
     * @param id language's id
     * @return
     */
    public static Config getLanguageById(String id) {
        return languages.getOrDefault(id, languages.get("en"));
    }
}
