<div align="center">
    <img src="https://i.imgur.com/5mFrv5m.jpg" height="200" width="200">
    <h1>Puffin</h1>
    <strong>Discord bot with leveling system and more!</strong><br><br>
    <img src="https://forthebadge.com/images/badges/made-with-java.svg" height="30">&nbsp;
    <img src="https://forthebadge.com/images/badges/built-with-love.svg" height="30">&nbsp;
    <a href="https://discord.gg/rfsEqme"><img src="https://img.shields.io/discord/595244404386824207.svg?style=for-the-badge" height="30"></a>&nbsp;
</div>

---

**Puffin** is an open-source Discord bot written in Java, using library JDA.
Puffin provides very much of functions, exmaple leveling, banning with banlist and more!

## Inviting puffin
Click on the following [link](https://discordapp.com/oauth2/authorize?client_id=595218667592220672&scope=bot&permissions=2139090167) to add Puffin on your server.

The official website for Puffin can be founded is https://puffnbot.xyz/.

## Main Contributors
* [Speedy11CZ#0001](https://github.com/Speedy11CZ)

*Puffin and this README were inspired by [CorgiBot](https://github.com/CorgiBot/)*